#ifndef LIB_SCOPE_GUARD_HPP
#define LIB_SCOPE_GUARD_HPP

#include <utility>

template<typename Func>
class scope_guard
{
public:
    scope_guard() = delete;
    scope_guard(scope_guard const&) = delete;

    template<
        typename L,
        typename disable_copy_ctor<scope_guard, L>::type* = nullptr>
    scope_guard(L&& l)
    : m_dismissed(false)
    , m_rollback(std::forward<L>(l))
    {}

    scope_guard(scope_guard&& o)
    : m_dismissed(o.m_dismissed)
    , m_rollback(std::move(o.m_rollback))
    {
        o.dismiss();
    }

    scope_guard& operator=(scope_guard const&) = delete;

    bool dismissed() const { return m_dismissed; }
    void dismiss() { m_dismissed = true; }

    ~scope_guard()
    {
        if(!dismissed())
            m_rollback();
    }
private:
    template<typename C, typename Param>
    struct disable_copy_ctor
    : std::enable_if<
        !std::is_same<typename remove_rcv<Param>::type, C>::value>
    {};

    bool m_dismissed;
    Func m_rollback;
};

template<typename Func>
scope_guard<Func> make_scope_guard(Func func = Func())
{
    return scope_guard<Func>(func);
}

#endif
